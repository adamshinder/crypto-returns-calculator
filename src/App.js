import React, { useState } from "react";
import "./styles.css";
import Button from "@material-ui/core/Button";
import Box from "@material-ui/core/Box";
import TextField from "@material-ui/core/TextField";

import NativeSelect from "@material-ui/core/NativeSelect";

export default function App() {
  const [trade, setTrade] = useState({
    sellData: {},
    buyData: {},
    gains: 0,
    valid: false,
  });
  const coinList = [
    { id: 0, name: "None Selected" },
    { id: 1, name: "bitcoin" },
    { id: 2, name: "ethereum" },
    { id: 3, name: "tezos" },
    { id: 4, name: "cardano" },
  ];

  const [buyDate, setBuyDate] = useState("");
  const [sellDate, setSellDate] = useState("");
  const [coin, setCoin] = useState(coinList.name);
  const [volume, setVol] = useState(null);

  const calcGains = () => {
    setTrade({
      ...trade,
      gains:
        (trade.sellData.market_data?.current_price.usd -
          trade.buyData.market_data?.current_price.usd) *
        volume,
    });
  }; //... means keep all key/value pairs, only change what following

  const coingeckoUrl = (coin, date) => {
    return `https://api.coingecko.com/api/v3/coins/${coin}/history?date=${date}&localization=false`;
  };

  const coingeckoFetch = async (buy, coin, date) => {
    fetch(coingeckoUrl(coin, date)).then((response) =>
      response.json().then((jsonData) => {
        if (buy) {
          setTrade({ ...trade, buyData: jsonData });
        } else {
          setTrade({ ...trade, sellData: jsonData });
        }
      })
    );
  };

  const handleBuyChange = (e) => {
    let val = e.target.value;
    setBuyDate(val);
    coingeckoFetch(true, coin, val);
  };

  const handleSellChange = (e) => {
    let val = e.target.value;
    setSellDate(val);
    coingeckoFetch(false, coin, val);
  };

  const handleCoinChange = (e) => {
    let val = e.target.value;
    setCoin(val);
    coingeckoFetch(null, coin, val);
  };

  return (
    <div className="App">
      <h1> 📈🚀 Cryptocurrency Returns Calculator 😢📉 </h1>
      <Box className="box">
        <div className="top-row">
          <Box>
            <h2> 🏦 Select Coin </h2>
            <NativeSelect
              defaultValue={coin}
              onChange={(val) => handleCoinChange(val)}
            >
              {coinList.map((item) => (
                <option key={item.id}>{item.name}</option>
              ))}
            </NativeSelect>
          </Box>
          <Box>
            <h2>Enter Buy Date </h2>

            <TextField
              placeholder="dd-mm-yyyy"
              format="dd-mm-yyyy"
              helperText="Insert date in the correct format"
              defaultValue={buyDate}
              onChange={(val) => handleBuyChange(val)}
            />
          </Box>

          <Box>
            <h2>Enter Sell Date</h2>
            <TextField
              placeholder="dd-mm-yyyy"
              helperText="Insert date in the correct format"
              className="text-inputs"
              defaultValue={sellDate}
              onChange={(val) => handleSellChange(val)}
            />
          </Box>
          <Box>
            <h2> 💰 Enter Amount of Tokens</h2>
            <TextField
              placeholder="Number of Tokens"
              className="text-inputs"
              value={volume}
              onChange={(e) => setVol(e.target.value)}
            />
          </Box>
        </div>
        <div className="bottom-row"></div>
        <Button variant="contained" color="primary" onClick={calcGains}>
          Calculate
        </Button>
        <h1 style={{ color: trade.gains < -1 ? "red" : "green" }}>
          🤑💸 Returns: ${trade.gains.toFixed(2)} USD 💸🤑
        </h1>
        <h3>
          You bought {trade.buyData.name} at:{" "}
          {trade.buyData.market_data?.current_price.usd.toFixed(2)} USD
        </h3>
        <h3>
          You sold {trade.buyData.name} at:{" "}
          {trade.sellData.market_data?.current_price.usd.toFixed(2)} USD
        </h3>
        <h4 className="text-box">
          {" "}
          If the calculator doesn't calculate your returns, re-check the
          accuracy of the dates you input and the format (dd-mm-yyyy). Always
          restart the application in between calculations. Start from the left
          to right when inputting the data. As this is a tutorial, continue
          building this and make it better! Some ideas include adding a
          percentage on your gains, change the emojis based on the results,
          and/or conditionally render the "USD" and other text elements. I hope
          you learned something, and thank you for using the crypto calculator!
          Reach out to me on Twitter at{" "}
          <a href="https://twitter.com/adshinder">@adshinder</a> if you have any
          questions!
        </h4>
      </Box>
    </div>
  );
}

//  <input defaultValue={coin} onChange={(val) => handleCoinChange(val)} />

//<h3>{trade.buyData.market_data?.current_price.usd.toFixed(2)} USD</h3>
//<h3>{trade.sellData.market_data?.current_price.usd.toFixed(2)} USD</h3>

// <MuiPickersUtilsProvider utils={DateFnsUtils}>
//    </MuiPickersUtilsProvider>

//Add a percentage gains, change the color based on dubs or l's, conditionally render the "USD"

// console.log("jD:", jsonData);
//console.log("buy:", buy);
